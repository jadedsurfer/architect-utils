"use strict";
var debug = require("debug");
var Emitter = require("wildemitter");

module.exports = function(options, imports, register) {
  var log = debug("utils");
  log("start");

  var api = {
    debug: debug,
    broadcaster: new Emitter(),
    fs: {
      getDirectories: function (basePath, cb) {
        var fs = require("fs");
        var path = require("path");
        var directories = [];

        fs.readdir(basePath, function (err, files) {
          if (err) {
            throw err;
          }

          files.map(function (file) {
            return path.join(basePath, file);
          }).filter(function (file) {
            return fs.statSync(file).isDirectory();
          }).forEach(function (file) {
            directories.push(file);
          });

          cb(null, directories);
        });
      }
    },
    array: {
      unique: function(array) {
        var a = array.concat();
        for(var i=0; i<a.length; ++i) {
          for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j]) {
              a.splice(j--, 1);
            }
          }
        }
        return a;
      }
    },
    logger: {
      debug: function(){

      }
    }
  };

  log("register");
  register(null, api);
};
